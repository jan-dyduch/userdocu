This workflow is for small changes, like correcting typos or adding a additional paragraph to a existing page.

## Prerequisits
* [**Gitlab-account:**](#creating-a-gitlab-account)
You need to have a gitlab-account to edit the [Userdocumentation-repository](https://gitlab.com/openCFS/userdocu) and create a merge request.

## How to edit in the browser
Just follow these steps in the gif and in no time you have contributed to the userdocumentation.

![](easyContribute.gif)

Here the workflow from the gif above:

1. Click on the edit button
2. Log in
3. Click on edit in browser
4. Fork project
5. Edit it in browser
6. Preview in browser
7. Add commit message and commit changes
8. Add description to merge request
9. Click create merge request
10. Let the pipeline check if everything is correct
11. If everything is ok, contact a maintainer (via email)

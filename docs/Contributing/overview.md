Contributing to the openCFS userdocu
====================================

We're happy about contributions to keep the documentation growing and up to date!
Below you find some details on how to do that.
If you have any questions and/or ideas how to improve the userdocumentation open an [issue](https://gitlab.com/openCFS/userdocu/-/issues).

**Please keep in mind, that this will be the documentation for users and not developers**

## Contributing small changes
Following workflow is advices if you only want to contribute small changes, like correcting typos and adding additional information.

[Click here to see how you can edit the userdocumentation via webbrowser](easyContribution.md)

## Contributing big changes

If you want to contribute big changes (like adding whole sections, rework the linking, etc.)
we suggest that you work locally.

[Click here to see how you can get started](advContribution.md)

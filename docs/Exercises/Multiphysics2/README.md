# Exercises

This section contains task descriptions of current and previous teaching courses.

Please see the menu on the left for the individual exercises. Each section contains

- the task description including sketches, and
- the necessary files for completing the exercise

There are no solutions given to the individual exercises because it is up to you to work through the different tasks in order to learn the correct handling of our workflow.
In case you are looking for some hands on tutorials with step-by-step solutions, it is refered to the [Tutorials](../../Tutorials/AnalysisWorkflow/README.md) section

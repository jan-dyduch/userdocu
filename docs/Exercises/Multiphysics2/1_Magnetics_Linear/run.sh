#!/bin/sh

####################################################################
# Linux shell script to automate the simulation workflow:
# The first line specifies that the 'bash'-shell should be used to 
# execute the file. Commands in the file are executed in order. 
# You can run the script in a terminal by: 
#   ./harmonic.sh
# Possibly you first need to make it executable using:
#   chmod +x harmonic.sh
####################################################################

# mesh with trelis
#trelis -batch -nographics -nojournal geometry-ue.jou

# run the simulation
for mat in 'air' 'iron' 'copper' 'alu';
do
  job=test_$mat
  # replace MAT in input-xml by air, iron,... and write a new input called $job.xml 
  #sed 's/MAT/'$mat'/g' magnet.xml > $job'.xml'
  # run cfs
  #cfs -p $job'.xml' $job
done

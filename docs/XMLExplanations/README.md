# XML-Input-Files

OpenCFS uses xml-files as input for simulations, materials and more. 
XML-files can be easily edited and can be autocompleted if the editor supports it.
In this section we will explain the basic structure of the xml-input, and how to obtain the [autocompletion feature](#xml-autocompletion).
Also [documentation inside the xml is available](#scheme-suggestion), although its not as complete as we wish.

To see which xml-editors we use please have a look at:

* [XML-Editors](../Installation/XML-Editors.md).

## XML-Structure
This is just a summary of the most important and common xml commands and structure, for a more detailled explanation, visit the PDE specific descriptions, look at the provided examples or use the documentation of the xml scheme itself, e.g. via oxygen or eclipse

### Skeleton of an openCFS xml input file
The underlying structure of our xml files is defined via a so-called xml-scheme.
The path to this scheme is defined in the root tag of every xml file by the `xsi:schemaLocation` attibute and should point to a valid location to enable [XML-autocompletion](#xml-autocompleting) in XML editors.
Here is an examle root tag:
```
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
```

In general, an OpenCFS xml file consists of three main parts:

* [fileFormats](#fileformats-section): definition of input-/output- and material-files
* [domain](#domain-section): region definitions (assign material to different regions)
* [sequenceStep](#sequencestep-section): contains the definition of the analysis type and the PDE. Several sequenceSteps can be concatenated and results from previous sequenceSteps can be processed in order to produce the desired result.

<details>
  <summary>Click to see a typical xml-input</summary>
```
<!-- define which files are needed for simulation input & output-->
<fileFormats>
    <input>
        <!-- read mesh -->
    </input>
    <output>
        <!-- define output -->
    </output>
    <materialData file="../material/mat.xml" format="xml"/>
</fileFormats>

<domain geometryType="3d">
    <regionList>
        <!-- region definition -->
    </regionList>
</domain>

<sequenceStep index="1">
    <analysis>
        <!-- analysis type: static, transient, harmonic, multiharmonic, eigenfrequency -->
    </analysis>
    <pdeList>
        <!--for example consider the electric conduction PDE-->
        <elecConduction>
            <regionList>
                <!--define on which regions the PDE is solved-->
            </regionList>
            <bcsAndLoads>
                <!--define appropriate BC's-->
            </bcsAndLoads>
            <storeResults>
                <!--define the results to be stored-->
            </storeResults>
        </elecConduction>
    </pdeList>
</sequenceStep>
</cfsSimulation>
```
</details>

### Detailed Description
In the following the most important, PDE-independent, tags are described.
For a more detailled explanation, visit the PDE specific descriptions, look at the provided examples or use the documentation of the xml scheme itself, e.g. via oxygen or eclipse


#### `fileFormats` Section

* `<input>` (mandatory) defines which files are read as input (either data and/or mesh)
* Different inputs are possible `gmsh`, `cdb`, `ensight`, `hdf5`. Several more are available in the xml scheme but they are not maintained
* When reading more than one file, unique `id's` have to be provided
* `scaleFac=` (optional) scales the provided mesh with a prescribed factor, e.g. conversion from mm to m
* `<scatteredData>` (optional) can read some pointwise data, for example from a csv file
* `<output>` (mandatory) defines the output format, most of the time you want to export the results in h5 format, sometimes also in text format, e.g. global integrated quantities
* `<materialData>` (mandatory) defines the location of the material file

<details>
  <summary>Click to see a fileFormats-snippet</summary>
```
<fileFormats>
    <input>
      <gmsh fileName="../meshes/model_indwat.msh2" id="i1" scaleFac="1e-3"/>
      <gmsh fileName="../meshes/model_air.msh2" id="i2"/>
      <gmsh fileName="../meshes/model_sheet.msh2" id="i4"/>
    </input>
    <scatteredData> 
        <csv fileName="testinput.csv" id="inCSV">
            <coordinates>
                <comp dof="x" col="1"/>
                <comp dof="y" col="2"/>
                <comp dof="z" col="3"/>
            </coordinates>
            <quantity name="heatSourceDensity" id="hsd1">
                <comp dof="x" col="0"/>
            </quantity>
        </csv>
    </scatteredData>
   <output>
      <hdf5/>
      <text id="txt"/>
   </output>
   <materialData file="../material/mat.xml" format="xml"/>
</fileFormats>
```
</details>

#### `domain` Section

* `<regionList>` (mandatory) define the material of every region in the mesh file(s)
<details>
  <summary>Click to see a regionList-snippet</summary>
```
  <domain geometryType="plane">
    <regionList>
      <region material="air" name="region_1"/>
      <region material="iron" name="region_2"/>
      <region material="air" name="region_3"/>
    </regionList>
  </domain>
```
</details>
#### `sequenceStep` Section

* `<analysis>` (mandatory) define the kind of analysis to be performed (static, harmonic, transient…).
* Only one analysis type can be done per sequence step.
* `<pdeList>` (mandatory) contains all pde's used in this sequence step. There can be multiple pde's in one sequence step e.g. if you use direct coupling.
    * `<pdeName>` (mandatory) defines one PDE and all needed information
        * [`<regionList>`](#details-to-regionlist) (mandatory) define the regions where that pde is defined and computed on
        * [`<bcsAndLoads>`](#details-to-bcsandloads) (mandatory) define the boundary conditions (dirichlet BC, neumann BC, ...) and loads (heat source density, ...) of this pde
        * [`<storeResults>`](#details-to-storeResults) (mandatory) define the results that we want to get from the analysis (e.g. Temperature, heat flux density, ...)
<details>
  <summary>Click to see a sequenceStep-snippet</summary>
```
<sequenceStep index="1">
    <analysis>
      <harmonic/>
      <transient/>
      <static/>
    </analysis>
    <pdeList> 
      <heatConduction>
        <regionList>
          <region name="S_wall"/>
        </regionList>
        <bcsAndLoads>
          <temperature name="L_left" value="25"/>
          <temperature name="L_right" value="-10"/>
        </bcsAndLoads>   
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions/>
          </nodeResult>
          <elemResult type="heatFluxDensity">
            <allRegions/>
          </elemResult>
        </storeResults>
      </heatConduction>
    </pdeList>  
  </sequenceStep>
```
</details>
### Details to regionList
* Insert additionally information here 
### Details to bcsAndLoads
* For more details regarding the definition of loads, please refer to [Math Expressions](../Tutorials/Features/mathexpressions.md).
* For specific boundary conditions and loads please refer to the specific [PDE Explanations](../PDEExplanations/Singlefield/AcousticPDE/README.md)
### Details to storeResults
* For more details on obtaining results at specific locations, please refer to [Results at specific points](../Tutorials/Features/sensors.md).
* Note that the different kinds of results (mechanic deformation, electric potential, acoustic pressure etc.) and loads (pressure, heat flux etc.) available to select in the first place depend on the physical system. They are described for the different systems in detail under [PDE Explanations](../PDEExplanations/Singlefield/AcousticPDE/README.md).
    
There are various result-types in openCFS, which can be specified in the xml-file (node-, element-, region-, surfaceElement-, surfaceRegion-results).
<details>
  <summary>Click to see a storeResults-snippet</summary>
```
<storeResults>
    <elemResult type="">
        <allRegions/>
    </elemResult>
    <nodeResult type="">
        <allRegions/>
    </nodeResult>
    <regionResult type="">
        <allRegions/>
    </regionResult>
    <regionResult type="">
        <allRegions/>
    </regionResult>
    <surfElemResult type="">
        <allRegions/>
    </surfElemResult>
    <surfRegionResult type="">
        <allRegions/>
    </surfRegionResult>
</storeResults>
```
</details>
The type of your desired quantity depends on the type of the FE-result:

* A **primary** result corresponds to the degrees of freedom in the FE system. For example: The temperature $T$ in the heat conduction PDE is a primary result. The output is either on nodes (for nodal elements) or on elements (for edge elements).
* A **derived** result is computed from the primary by applying a certain operator, e.g., gradient, curl, divergence. For example: The heat flux density is the gradient of the temperature $-\nabla T$ in the heat conduction PDE.
The output is on elements (for nodal as well as edge elements)
* An **integrated** result is computed by integrating a result over a volume. For example, integrating the heat flux density over a volume, results in the total heat flux.
The output is stored in .hist files in the history-folder.

## XML-Autocompleting
If you have no autocompletion, probably the wrong xml-schema location is defined. Simply replace the header tag with the lines blow for the **material-xml**, **simulation-xml** and **cfsdat-xml**. 

### XML-Autocompletion if you have internet
* **Simulation-Input-XML**: Be sure that the header tag `<cfsSimulation ...>` looks like this:
```
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
```
* **Material-Input-XML**: Be sure that the header tag `<cfsMaterialDataBase ...>` looks like this:
```
<cfsMaterialDataBase xmlns="http://www.cfs++.org/material"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.cfs++.org/material 
https://opencfs.gitlab.io/cfs/xml/CFS-Material/CFS_Material.xsd" >
```
* **CFSDat-Input-XML**: Be sure that the header tag `<cfsdat ...>` looks like this:
```
<cfsdat xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Dat/CFS_Dat.xsd">
```

### XML-Autocompletion if you have no internet
If you have to work offline, you have to define where the schemas can be found. The schemas are automatically distributed with the cfs-installation and located under `localPathToCFS/cfs/share/xml/...`.
IF you do not know the installation path of openCFS you can simply type this terminal command (on ubuntu):
```
which cfs
```
* **Simulation-Input-XML**: Be sure that the header tag `<cfsSimulation ...>` looks like this. **Attention: You have to adapt to the path of your local cfs-installation**:
```
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
local/Path/to/cfs/share/xml/CFS-Simulation/CFS.xsd">
```
* **Material-Input-XML**: Be sure that the header tag `<cfsMaterialDataBase ...>` looks like this. **Attention: You have to adapt to the path of your local cfs-installation**:
```
<cfsMaterialDataBase xmlns="http://www.cfs++.org/material"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.cfs++.org/material 
local/Path/to/cfs/share/xml/CFS-Material/CFS_Material.xsd" >
```
* **CFSDat-Input-XML**: Be sure that the header tag `<cfsdat ...>` looks like this. **Attention: You have to adapt to the path of your local cfs-installation**:
```
<cfsdat xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
local/Path/to/cfs/share/xml/CFS-Dat/CFS_Dat.xsd">
```

## Scheme Suggestions
In case you don't know our openCFS xml-scheme by heart, eclipse's xml plugin helps you and suggests the appropriate tags and elements...if you ask for it.
How you do that, is described in the following.

For example let us consider we want to know which boundary conditions and loading options exist in a certain PDE.
Just navigate to the <bcsAndLoads> press Enter for a linebreak, open a new tag with the '<' symbol and wait a second.
Then you get the possible boundary conditions suggestions together a brief description.

![a7](xmleclipse3.png)

On the other hand it is also possible to display the element options inside a tag, therefore just navigate inside a tag (between < and > symbol), insert a blank space and press 'crtl+Space', wait a second and you will see the possible elements together with a brief description.

![a8](xmleclipse4.png)


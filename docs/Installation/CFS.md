# Installing openCFS

openCFS runs on Linux, macOS and Windows.
You can download binary _Packages_ for most [releases](https://gitlab.com/openCFS/cfs/-/releases).
Below you find instructions for your operating system.

## Linux
We provide openCFS including all dependencies in a single archive.
The software should run on any recent Linux system.

To install, just download the most recent archive, e.g. the latest release package [Linux binary (tar.gz)](https://gitlab.com/openCFS/cfs/-/releases/permalink/latest/downloads/CFS-Linux.tar.gz), and extract it to the desired location.

```shell
wget https://gitlab.com/openCFS/cfs/-/releases/permalink/latest/downloads/CFS-Linux.tar.gz
tar -xzvf CFS-Linux.tar.gz
```

This will extract to `CFS-<release>-Linux` where `<release>` denotes the version.
The executables are found in the `bin` directory of the installation, e.g. in the directory `CFS-<release>-Linux/bin`.
You can add this directory to your `PATH` by running

```
export PATH=<absolute-cfs-installation-path>/bin:$PATH
```

To make this persistent add it to the configuration file of your shell, e.g. to `~/.bashrc` for bash.

You can now run cfs by
```shell
cfs -h
```

## macOS
We provide openCFS including a subsection of dependencies in a single archive.
For structural optimization you shall download the snopt and scip source from the respective code authors and compile openCFS yourself.

The provided binary is built with recent macOS (Intel) and not tested on former platforms.

For further help please follow the Linux instructions

Download a snapshot from March 2021 via this link [CFS-macOS_2021_03.tar.gz](https://faubox.rrze.uni-erlangen.de/getlink/fi7ctatGvtFGcAcSKQSKp1KN/CFS-macOS_2021_03.tar.gz)


## Windows

To install openCFS on windows first download the latest portable [Windows release](https://gitlab.com/openCFS/cfs/-/releases/permalink/latest/downloads/CFS-Windows.zip).
After downloading extract it to the desired location, for example to `C:\Users\username\CFS\`. 
You can now follow the instructions below to add the openCFS directory to the PATH to be able to use it in the command prompt:

- Open **Control Panel** 
- In upper right corner in the search bar type in *edit environment variables*.
- Select *Edit environment variables for your account* if you want to make it accessible only to your account.
- In the upper window part you will see a Variable named `Path`, double click on it.
- Copy the path to the `bin` directory from where you extracted the openCFS archive (e.g. : `C:\Users\username\CFS\bin`).
- In the new opened window click on *New* and paste the path that you previously coppied.
- Click on _ok_ after pasting and then again _ok_ on the *Environment variables* window.
- You should be done now, to test it open a **command prompt** and type `cfs --help`, you should see an openCFS help page in your command prompt.

### Windows - WSL
An alternative for running openCFS on Windows is the usage of [WSL](https://docs.microsoft.com/en-us/windows/wsl/). It lets one run a Linux environment on Windows without the overhead of a VM, or dual-boot setup. 
For the installation, please follow the provided guide [Manual-WSL-Installation](https://docs.microsoft.com/en-us/windows/wsl/install-win10#manual-installation-steps). It is recommended to use the Ubuntu distribution, since this is tested, but it should run on all distribution.   
After the installation of WSL and the distribution the Linux guide on installing openCFS can be followed.   
Please note that it could be necessary to change the permissions of the cfs folder. This can be done with ```sudo chmod -R 755 /cfs_dir```.   

It is possible that a shared library is missing from within the ubunut WSL (```libgomp.so.1```). It can be installed with ```sudo apt install libgomp1```

Afterwards it is possible to call openCFS from within powershell via ```wsl bash -c -i cfs --help```

To get GUI applications running in WSL, one can use [XLaunch](https://x.cygwin.com/docs/xlaunch/). For further details, there are different tutorials like [1](https://dev.to/egoist/running-linux-gui-programs-in-wsl2-29j3), [2](https://www.artemix.org/blog/linux-softwares-on-windows.html).

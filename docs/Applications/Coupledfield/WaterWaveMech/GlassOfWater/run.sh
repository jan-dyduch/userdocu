#!/bin/bash
# run all ...

cubit -nographics -nojournal -batch GlassOfWater.jou

# run the eigenfrequency analysis in CFS
cfs WaterModes

# open Paraview and load result
# paraview results_hdf5/WaterModes.cfs

# run the harmonic analysis in CFS
cfs -t 4 Forced_Sway

# for the docs
jupyter nbconvert GlassOfWater.ipynb --to markdown
mv GlassOfWater.md README.md

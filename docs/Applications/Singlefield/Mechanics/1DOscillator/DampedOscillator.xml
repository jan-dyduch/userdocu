<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
  xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>Damped 1D Oscillator</title>
    <authors>
      <author>Ben Heinrich</author>
    </authors>
    <date>2023-01-11</date>
    <keywords>
      <keyword>mechanic</keyword>
      <keyword>eigenValue</keyword>
    </keywords>
    <references></references>
    <isVerified>yes</isVerified>
    <description>
    This input file defines a single degree of freedom oscillator composed of a square mass (1mx1m) with density 1 kg/m3 in 2d (plane strain).
    We constrain in x direction and connect a concentrated stiffness and damping in the y direction.

    The resulting system has a mass m=1; we set k=1, thus, the (undamped) natural frequency is w_0=2*pi Hz.
    Setting xi=3/5, i.e. the damping constant c=2*m*w_0*xi=2*1*1*3/5, we obtain 2 eigenvalues with 
    Re(lambda) = -3/5 = -0.6 and Im(lambda) = +-4/5 = +-0.8
    
    The corresponding quadratic EVP is linearized with the quadratic eigensolver, the resulting generalized EVP is solved with the external eigensolver 
    (calling a Python script). The eigenvalues and eigenvectors are imported from a matrix market file in coordinate format.
    </description>  
  </documentation>
  
  <fileFormats>
    <input>
      <gmsh fileName="UnitSquare.msh"/> 
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType='plane'  printGridInfo="no">
    <regionList>
      <region name="V_square" material="UnitMaterial" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <eigenValue>
        <valuesAround>
          <shiftPoint>
            <Real>-0.6</Real>
            <Imag>0</Imag>
          </shiftPoint>
          <number>2</number>
        </valuesAround>
        <eigenVectors normalization="norm" side="right"/>
        <problemType>
          <Quadratic>
            <quadratic>mass</quadratic>
            <linear>damping</linear>
            <constant>stiffness</constant>
          </Quadratic>
        </problemType>
      </eigenValue>
    </analysis>  
    
    <pdeList>
      <mechanic subType='planeStrain'>
        <regionList>
          <region name="V_square"/>
        </regionList>        
        <bcsAndLoads>
          <fix name="S_fixed">
            <comp dof="x"/>
          </fix>
          <concentratedElem name="N_concentrated-element" dof="y" stiffnessValue="1.0" dampingValue="3/5*2"/>
        </bcsAndLoads>      
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <eigenSolver id="myquadratic"/> <!--one can switch between eigensolvers-->
          </standard>
        </solutionStrategy>
        <eigenSolverList>
          <quadratic id="myquadratic">
            <generalisedEigenSolver id="ext"/>
            <linearisation>firstCompanion</linearisation>
          </quadratic>
          <external id="ext">
            <logging>yes</logging>
            <cmd>python3 EigenSolver.py</cmd>
            <arguments>
              <AFileName/>
              <BFileName/>
              <shiftPoint/>
              <number/>
              <tolerance>1e-9</tolerance>
              <EigenValuesFileName/>
              <EigenVectorsFileName/>
            </arguments>
          </external>
        </eigenSolverList>
      </system>
    </linearSystems>
  </sequenceStep>
  
</cfsSimulation>

# Math Expressions

The math expression parser allows to use simple mathematical expressions in combination with if statements inside of the xml files of openCFS. For this user-defined variables can be used

```
<domain>
  <variableList>
    <var name="A" value="1e6"/>
    <var name="B" value="1500"/>
  </variableList>
</domain>
```
The registered variables get defined in the global scope and can be used
in any expression, which gets parsed by the mathParser. Note however, that
for establishing user-defined variables computed variables such as `t` can not be used. 
Using user-defined variables for defining an additional user-defined variable however, works. 
```
<domain>
  <variableList>
    <var name="C" value=A*B/>
  </variableList>
</domain>
```
In addition, the following pre-defined variables can be found in openCFS:

* `t` - Current time value, only available for transient simulations
* `dt` - Time step size, only available for transient simulations
* `f` - Current frequency value, only available for harmonic simulations
* `step` - Current time/frequency step, available for harmonic and transient simulations
* `x,y,z` - Coordinates
* `r,phi,z` - Cylindrical coordinates



## Defining loads 
Math expressions can be used to define a synthetic load. For the following example a transient acoustic simulation (Singlefield -> AcousticPDE) is considered. The load is impressed onto a certain node, and defined as a sinus function 

```
 <bcsAndLoads>
    <pressure name="excite" value="2*sin(2*pi*100*t)"/>
 </bcsAndLoads>
```
In this example a sinus load with 100 Hz and an amplitude of 2 is defined. $t$ denotes the control variable. A different possibility for defining such a synthetic load is by using one of the custom functions of openCFS:

* `spike(duration, timeval)` - Generates a short spike signal of a given duration starting from 0.
* `fadeIn(duration, mode, timeval)` - This function is used to perform a fadein of an excitation. The returned value is between zero and one and has to be used as envelope of the excitation signal (mode: see detailed explanation below)
* `sinBurst(freq, nperiods, nfadein, nfadeout, timeval)` - Creates data for a sine burst signal. A sine burst signal denotes a sine that will be active during a certain amount of time until it will be deactivated. Additionally, a fade-in/out can be applied, according to the sin^2 method of the fadeIn to reduce the higher harmonics of the signal.
* `squareBurst(freq, nperiods, bipolar, pwidth, rtime, timeval)` - Create data for a square burst signal, which can either be unipolar or bipolar.
* `gauss(mue, sigma, normval, timeval)` - Generates a Gaussian curve, which is either normalized to a maximum value of 1 or to having an area of 1.
* `locCoord2D(coordsysid, component, x, y)` - This function returns for a point in a 2D cartesian coordinate system one component of a local coordinate system, as defined in the < coordSysList >-section of the < domain >-element (for details, see 4.4). This can be used,e.g. to define loads / dirichlet boundary conditions depending on polar / cylindrical coordinates.
* `locCoord3D(coordsysid, component, x, y,z)` - Similar to the function locCoord2D, this method can be used for local 3-dimensional coordinate systems (e.g. cylindrical or spherical).
* `sample1D(filename, sampleval, interpolation)` - This function can be used to read in and interpolate values of a 2-column formatted text file. This can be used e.g. to prescribe space/time/frequency-dependent boundary or load conditions.
* `if(condition, trueval, elseval)` - if ... then ... else ...
* `sin(2 * pi * frequency * timeStep)` 
* `cos(2 * pi * frequency * timeStep)` 
* `tan(2 * pi * frequency * timeStep)` 
* `asin(2 * pi * frequency * timeStep)` 
* `acos(2 * pi * frequency * timeStep)` 
* `atan(2 * pi * frequency * timeStep)` 
* `sinh(2 * pi * frequency * timeStep)` 
* `cosh(2 * pi * frequency * timeStep)` 
* `tanh(2 * pi * frequency * timeStep)` 
* `asinh(2 * pi * frequency * timeStep)`
* `acosh(2 * pi * frequency * timeStep)`
* `atanh(2 * pi * frequency * timeStep)`
* `log2( X )` - logarithm to the base 2 
* `log10( X )` - logarithm to the base 10 
* `log( X )` - logarithm to the base 10 
* `ln( n )` - logarithm to base e (2.71828...) 
* `exp( X )` - e raised to the power of x 
* `sqrt( X )` - square root of a value 
* `sign( X )` - sign function -1 if x < 0; 1 if x > 0 
* `rint( X )` - round to nearest integer
* `abs( X )` - absolute value  
* `min( X )` - min of all arguments
* `max( X )` - max of all arguments
* `sum( X )` - sum of all arguments 
* `avg( X )` - mean value of all arguemnts 




Additionally, the following built-in operators and constants are available: 

* `=` - Assigment
* `and` - logical and
* `or` - logical or
* `xor` - logical xor
* `<= or le *` - less or equal
* `>= or ge *` - greater or equal
* `!=` - not equal
* `==` - equal
* `> or gt *` - greater than
* `< ot lt *` - less then
* `+` - addition
* `-` - subtraction
* `*` - multiplication 
* `/` - division
* `^` - raise to the power of
* `pi or _pi` - 3.1415926..
* `_e` - Eulerian number e = 2.7182818..




## Defining fields

Math expressions can also be used for defining e.g flow, or temperature fields. 
```
 <flowList>
  <flow name="backward_Z"> 
   <comp dof="x" value="0"/>
   <comp dof="y" value="120000/60*2*pi*z"/> 
   <comp dof="z" value="-120000/60*2*pi*y"/>
  </flow>
 </flowList>
```
This can also be used for defining blending functions, which e.g. necessary for the usage of non-conforming interfaces in combination with aeroacoustic source terms. For more details look into Singlefield -> AcousticPDE

## Detailed explanations and examples

Following we will show two different examples for such custom functions: 

### Spike
`spike(duration, timeval)` 

* duration -> Duration of spike in s. 
* timeval -> Time parameter at which function gets evaluated. Usually the time variable t is used.

Example for spike signal: spike(1,t-2)

![spike](spike.png)



### fadeIn 
`fadeIn(duration, mode, timeval)`

* duration -> The amount of time for which the fade in operation is performed in s.
* mode -> Three different implementation exist for the envelope of the fade in process:

                 mode = 1 (using a sin^2 formulation) 
                 mode = 2 (using an exponential formulation exp)
                 mode = 3 (using a squared exponential formulation exp^2)
* timeval -> Time parameter at which function gets evaluated. Usually the time variable t is used.


Comparison of 3 fadeIn signals: sin 2 -based (red), exp-based (green) and exp 2 -based (blue):

![fadein](fadein.png)



### sinBurst
`sinBurst(freq, nperiods, nfadein, nfadeout, timeval)`

* freq -> Frequency of the sine pulse.
* nperiods -> Total number of periods of the pulse.
* nfadein -> Specifies the number of periods that will be used for the transient process. This will realize a kind of ”soft” transient process that will reduce the higher harmonics of the resulting signal.
* nfadeout -> Specifies the number of periods that will be used for the decay process. This will realize a kind of ”soft” decay process that will reduce the higher harmonics of the resulting signal.
* timeval -> Time parameter at which function gets evaluated. Usually the time variable t is used.


Sine burst without fade-In: sinBurst(1,6,0,0,t):

![sinburst](sinburst.png)


Sine burst with 2 periods of fade-in/out: sinBurst(1,6,2,2,t)

![sinburst2](sinburst2.png)



### squareBurst
`squareBurst(freq, nperdios, bipolar, pwidth, rtime, timeval)`

* freq -> Frequency of the rectangular signal.
* nperiods -> Total number of periods of the pulse.
* bipolar -> Switch for generating bipolar / unipolar signal; 0: unipolar signal (0 ≤ y ≤ 1); 1: bipolar signal (−1 ≤ y ≤ 1)
* pwidth -> Pulse width of the signal in percent (0 ≤ pw ≤ 100).
* rtime -> Defines the rise time of the square burst. It denotes the time in seconds that the pulse will take to change from low to high level.
* timeval -> Time parameter at which function gets evaluated. Usually the time variable t is used.


![squareburst](squareburst.png)



### gauss
`gauss(mue, sigma, normval, timeval)`

* mue -> Mean value of Gauss curve.
* sigma -> Variance of Gauss curve.
* normval -> Type of normalization: 0: Maximum of curve gets normalized to 1; 1: Area of curve gets normalized to 1
* timeval -> Time parameter at which function gets evaluated. Usually the time variable t is used.



### locCoord2
`locCoord2D(coordsysid, component, x, y)`

* coordsysid Id -> string of the referred coordinate system.
* component -> Coordinate component of the local coordinate system, which can be either 1 or 2 in 2D. For a polar coordinate system, 1=r-direction and 2=phi-direction.
* x -> x-coordinate of the Cartesian point. For < load > and < dirichletInhom > boundary conditions, it can be literally replaced by x.
* y -> y-coordinate of the Cartesian point. For < load > and < dirichletInhom > boundary conditions, it can be literally replaced by y.


In the following example, a cylindrical coordinate system with id mid is
defined. An inhomogeneous Dirichlet boundary condition for an electrostatic
analysis is applied, with a value varying with the angle phi (second component
in a polar coordinate system).

```
<domain>
  <coordSysList>
    <polar id="mid">
      <origin x="0" y="0"/>
      <rAxis x="1" y="0"/>
    </polar>
  </coordSysList>
</domain>
....
<potential name=".." value="locCoord(’mid’,2,x,y)"/>
```


### locCoord3
`locCoord3D(coordsysid, component, x, y,z)`

* coordsysid Id -> string of the referred coordinate system.
* component -> Coordinate component of the local coordinate system, which can be either 1, 2 or in 3D. For a cylindrical coordinate system, 1=r-direction, 2=phi-direction and 3=z-direction.
* x -> x-coordinate of the Cartesian point. For < load > and < dirichletInhom > boundary conditions, it can be literally replaced by x.
* y -> y-coordinate of the Cartesian point. For < load > and < dirichletInhom > boundary conditions, it can be literally replaced by y.
* z -> z-coordinate of the Cartesian point. For < load > and < dirichletInhom > boundary conditions, it can be literally replaced by z.



### sample1D
`sample1D(filename, sampleval, interpolation)`

* `filename` is the name of the file to be read (any path in the filename is taken relative to the location of the .xml-file). The file must consist of exactly 2 columns, which are separated by whitespace (space, tab).
* `sampleval` denotes the sample value (value of the 1st column ), for which a value in the 2nd column is seeked. In most cases either the current time t or a coordinate component (x, y, z) may be used.
* `interpolation` selects the one of 3 different methods for interpolating the sampled values: 

        0: Nearest neighbor is taken (i.e. no interpolation at all)
        1: Linear interpolation
        2: Cubic spline interpolation
        

We can prescribe boundary conditions with values read in from sample files.
The notation is as follows:
```
<bcsAndLoads>
  <potential name="transducer" value="sample1D('x_amplitude.dat',x,2)" phase="sample1D('x_phase.dat',x,2)"/>
</bcsAndLoads>
```
Sample data (2 nd column) is read in from the files x amplitude.dat and
x phase.dat depending on the current x-coordinate and gets interpolated
using cubic spline interpolation. Please note the type of quotes used!


### if statement: 


```
 <bcsAndLoads>
    <pressure name="excite" value="(t lt 1)? (cos(2*pi*(t-0.5))+1)*sin(2*pi*t): 0")"/>
 </bcsAndLoads>
```


Furthermore, it is possible to superpose multiple different custom functions, with e.g. if statements. In the following example we superpose a time depending if statement with a spatial if statement
```
 <bcsAndLoads>
    <pressure name="exciteSurface" value="((t lt 1)? (cos(2*pi*(t-0.5))+1)*sin(2*pi*t): 0"))*((x lt 1)? (1 : 0))"/>
 </bcsAndLoads>
```

Hint: With multiple if statements it is especially important to look after the positioning of braces.
